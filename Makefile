.PHONY: test
test:
	@go test ./... -cover

.PHONY: test-report
test-report:
	@go test ./... -coverprofile=coverage.out && go tool cover -html=coverage.out
