module gitlab.qleanlabs.ru/platform/infra/qleanctl

go 1.16

require (
	github.com/AlecAivazis/survey/v2 v2.2.16
	github.com/dustin/go-humanize v1.0.0
	github.com/mitchellh/go-homedir v1.0.0
	github.com/spf13/cobra v1.2.1
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
	k8s.io/api v0.22.1
	k8s.io/apimachinery v0.22.1
	k8s.io/client-go v0.22.1
)
