package config

import (
	"io/ioutil"
	"os"
	"path"

	"github.com/mitchellh/go-homedir"
	"gopkg.in/yaml.v3"
)

var (
	homeDir    string
	configFile string
)

type Config struct {
	PreviousContext string `yaml:"previousContext"`
}

func init() {
	home, err := homedir.Dir()
	if err != nil {
		panic(err)
	}
	homeDir = home
	configFile = path.Join(homeDir, ".ql", "config")
}

func Load() (*Config, error) {
	c := &Config{}

	b, err := os.ReadFile(configFile)
	if err != nil {
		return c, nil
	}

	err = yaml.Unmarshal(b, c)
	return c, err
}

func (c *Config) Save() error {
	b, err := yaml.Marshal(c)
	if err != nil {
		return err
	}
	_ = os.MkdirAll(path.Dir(configFile), 0755)
	return ioutil.WriteFile(configFile, b, 0644)
}
