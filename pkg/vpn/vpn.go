package vpn

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"os"
	"regexp"
	"sort"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/remotecommand"
)

const (
	namespace = "openvpn"
)

var (
	openvpnLabelSelector = metav1.ListOptions{
		LabelSelector: "app=openvpn,release=openvpn",
	}
)

func FindOpenVPNPod(clientset kubernetes.Interface) (*corev1.Pod, error) {
	podList, err := clientset.CoreV1().Pods(namespace).List(context.Background(), openvpnLabelSelector)
	if err != nil {
		return nil, err
	}
	if len(podList.Items) == 0 {
		return nil, errors.New("openvpn pod not found")
	}
	return &podList.Items[0], nil
}

func FindOpenVPNPodName(clientset kubernetes.Interface) (string, error) {
	pod, err := FindOpenVPNPod(clientset)
	if err != nil {
		return "", err
	}
	return pod.Name, nil
}

func FindOpenVPNService(clientset kubernetes.Interface) (*corev1.Service, error) {
	serviceList, err := clientset.CoreV1().Services(namespace).List(context.Background(), openvpnLabelSelector)
	if err != nil {
		return nil, err
	}
	if len(serviceList.Items) == 0 {
		return nil, errors.New("openvpn service not found")
	}
	return &serviceList.Items[0], nil
}

func ListCertificates(clientset kubernetes.Interface, config *rest.Config) ([]string, error) {
	podName, err := FindOpenVPNPodName(clientset)
	if err != nil {
		return nil, err
	}

	cmd := []string{
		"sh",
		"-c",
		"ls -la /etc/openvpn/certs/pki/issued/*.crt",
	}
	revokeReq := clientset.CoreV1().RESTClient().
		Post().Resource("pods").
		Name(podName).
		Namespace(namespace).
		SubResource("exec").
		VersionedParams(&corev1.PodExecOptions{
			Command: cmd,
			Stdin:   true,
			Stderr:  true,
			Stdout:  true,
		}, scheme.ParameterCodec)
	exec, err := remotecommand.NewSPDYExecutor(config, "POST", revokeReq.URL())
	if err != nil {
		return nil, err
	}

	var (
		stdout bytes.Buffer
		stderr bytes.Buffer
	)
	err = exec.Stream(remotecommand.StreamOptions{
		Stderr: bufio.NewWriter(&stderr),
		Stdin:  os.Stdin,
		Stdout: bufio.NewWriter(&stdout),
	})
	if err != nil {
		return nil, err
	}
	if len(stderr.Bytes()) != 0 {
		return nil, errors.New(stderr.String())
	}
	certsRE := regexp.MustCompile("issued/(.*).crt")
	matches := certsRE.FindAllStringSubmatch(stdout.String(), -1)
	var certs []string
	for _, match := range matches {
		if len(match) > 1 {
			certs = append(certs, match[1])
		}
	}
	sort.Strings(certs)
	return certs, nil
}

func RevokeCertificate(clientset kubernetes.Interface, config *rest.Config, name string) error {
	podName, err := FindOpenVPNPodName(clientset)
	if err != nil {
		return err
	}

	cmd := []string{
		"/etc/openvpn/setup/revokeClientCert.sh",
		name,
	}

	revokeReq := clientset.CoreV1().RESTClient().
		Post().Resource("pods").
		Name(podName).
		Namespace(namespace).
		SubResource("exec").
		VersionedParams(&corev1.PodExecOptions{
			Command: cmd,
			Stdin:   true,
			Stderr:  true,
			Stdout:  true,
		}, scheme.ParameterCodec)
	exec, err := remotecommand.NewSPDYExecutor(config, "POST", revokeReq.URL())
	if err != nil {
		return err
	}

	var stderr bytes.Buffer
	err = exec.Stream(remotecommand.StreamOptions{
		Stderr: bufio.NewWriter(&stderr),
		Stdin:  os.Stdin,
		Stdout: os.Stdout,
	})
	if err != nil {
		return err
	}
	if len(stderr.Bytes()) != 0 && bytes.Contains(stderr.Bytes(), []byte("error")) {
		return errors.New(stderr.String())
	}
	return nil
}

func CreateCertificate(clientset kubernetes.Interface, config *rest.Config, name string) ([]byte, error) {
	podName, err := FindOpenVPNPodName(clientset)
	if err != nil {
		return nil, err
	}

	service, err := FindOpenVPNService(clientset)
	if err != nil {
		return nil, err
	}

	if len(service.Status.LoadBalancer.Ingress) == 0 {
		return nil, errors.New("empty service load balancer ingress list")
	}
	ingressIP := service.Status.LoadBalancer.Ingress[0].IP

	cmd := []string{
		"/etc/openvpn/setup/newClientCert.sh",
		name,
		ingressIP,
	}

	createReq := clientset.CoreV1().RESTClient().
		Post().Resource("pods").
		Name(podName).
		Namespace(namespace).
		SubResource("exec").
		VersionedParams(&corev1.PodExecOptions{
			Command: cmd,
			Stdin:   true,
			Stderr:  true,
			Stdout:  true,
		}, scheme.ParameterCodec)
	exec, err := remotecommand.NewSPDYExecutor(config, "POST", createReq.URL())
	if err != nil {
		return nil, err
	}

	var (
		stdout bytes.Buffer
		stderr bytes.Buffer
	)
	err = exec.Stream(remotecommand.StreamOptions{
		Stderr: bufio.NewWriter(&stderr),
		Stdin:  os.Stdin,
		Stdout: bufio.NewWriter(&stdout),
	})
	if err != nil {
		return nil, fmt.Errorf("%v: %s", err, stdout.String())
	}
	if len(stderr.Bytes()) != 0 && bytes.Contains(stderr.Bytes(), []byte("error")) {
		return nil, errors.New(stderr.String())
	}
	return stdout.Bytes(), nil
}
