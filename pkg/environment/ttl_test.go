package environment

import (
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestParseTTL(t *testing.T) {
	tests := []struct {
		ttl string
		dur time.Duration
		err error
	}{
		{
			ttl: "2s",
			err: errors.New("unknown ttl format: 2s"),
		},
		{
			ttl: "3h",
			dur: 3 * time.Hour,
		},
		{
			ttl: "abcdh",
			err: errors.New("invalid ttl syntax: abcdh"),
		},
		{
			ttl: "7d",
			dur: 7 * 24 * time.Hour,
		},
		{
			ttl: "abcdd",
			err: errors.New("invalid ttl syntax: abcdd"),
		},
		{
			ttl: "9w",
			dur: 9 * 7 * 24 * time.Hour,
		},
		{
			ttl: "abcdw",
			err: errors.New("invalid ttl syntax: abcdw"),
		},
	}
	for _, test := range tests {
		d, err := ParseTTL(test.ttl)
		if test.err != nil {
			assert.Equal(t, test.err, err)
			continue
		}
		assert.NoError(t, err)
		assert.Equal(t, test.dur, d)
	}
}
