package environment

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

func ParseTTL(ttl string) (time.Duration, error) {
	if strings.HasSuffix(ttl, "h") {
		raw := strings.TrimSuffix(ttl, "h")
		hours, err := strconv.Atoi(raw)
		if err != nil {
			return 0, fmt.Errorf("invalid ttl syntax: %s", ttl)
		}
		d := time.Duration(hours) * time.Hour
		return d, nil
	}
	if strings.HasSuffix(ttl, "d") {
		raw := strings.TrimSuffix(ttl, "d")
		days, err := strconv.Atoi(raw)
		if err != nil {
			return 0, fmt.Errorf("invalid ttl syntax: %s", ttl)
		}
		d := time.Duration(days) * (24 * time.Hour)
		return d, nil
	}
	if strings.HasSuffix(ttl, "w") {
		raw := strings.TrimSuffix(ttl, "w")
		weeks, err := strconv.Atoi(raw)
		if err != nil {
			return 0, fmt.Errorf("invalid ttl syntax: %s", ttl)
		}
		d := time.Duration(weeks) * (7 * 24 * time.Hour)
		return d, nil
	}
	return 0, fmt.Errorf("unknown ttl format: %s", ttl)
}
