package version

import "fmt"

var (
	version = "v0.0.0-dev"
	commit  = ""
)

func GetVersion() string {
	if commit == "" {
		return version
	}
	return fmt.Sprintf("%s (%s)", version, commit)
}
