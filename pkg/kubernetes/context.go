package kubernetes

import (
	"sort"

	"k8s.io/client-go/tools/clientcmd"
)

func ListContext() (string, []string, error) {
	configAccess := clientcmd.ConfigAccess(clientcmd.NewDefaultPathOptions())
	config, err := configAccess.GetStartingConfig()
	if err != nil {
		return "", nil, err
	}
	var contexts []string
	for c := range config.Contexts {
		contexts = append(contexts, c)
	}
	sort.Strings(contexts)
	return config.CurrentContext, contexts, nil
}
