package ekill

import (
	"context"
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "ekill <namespace>",
		Short: "Kill Evicted pods",
		Long:  "Kill Evicted pods for a specific namespace.",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			namespace := args[0]

			client, err := kubernetes.NewClientSet()
			if err != nil {
				return err
			}

			listOpts := metav1.ListOptions{}
			pods, err := client.CoreV1().Pods(namespace).List(context.Background(), listOpts)
			if err != nil {
				return err
			}
			if len(pods.Items) == 0 {
				fmt.Println("No pods found")
				return nil
			}

			var podList []string
			for _, pod := range pods.Items {
				if pod.Status.Reason == "Evicted" {
					podList = append(podList, pod.Name)
				}
			}

			if len(podList) == 0 {
				fmt.Println("No Evicted pods found")
				return nil
			}

			podsInterface := client.CoreV1().Pods(namespace)
			for _, pod := range podList {
				err := podsInterface.Delete(context.Background(), pod, metav1.DeleteOptions{})
				if err != nil {
					return err
				}
				fmt.Printf("Pod %q deleted\n", pod)
			}
			return nil
		},
	}
	return cmd
}
