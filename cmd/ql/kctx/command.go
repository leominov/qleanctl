package kctx

import (
	"fmt"
	"os/exec"
	"strings"

	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/cobra"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/config"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
)

type options struct {
	printsCurrentContext bool
}

func Command(conf *config.Config) *cobra.Command {
	o := &options{}
	var cmd = &cobra.Command{
		Use:   "kctx",
		Short: "Switch Kubernetes context",
		Long: `Switch current Kubernetes context.

Useful alias for Shell:

alias kctx="ql kctx"`,
		RunE: func(cmd *cobra.Command, args []string) error {
			currentContext, contexts, err := kubernetes.ListContext()
			if err != nil {
				return err
			}
			if len(contexts) == 0 {
				fmt.Println("Empty context list, check your kube-config")
				return nil
			}
			if o.printsCurrentContext {
				fmt.Println(currentContext)
				return nil
			}
			contextName := ""
			if len(args) == 1 && args[0] == "-" {
				contextName = conf.PreviousContext
			} else if len(args) == 1 {
				var nextContext string
				for _, context := range contexts {
					if strings.Contains(context, args[0]) {
						nextContext = context
						break
					}
				}
				if nextContext == "" {
					fmt.Println("Context not found.")
					return nil
				}
				contextName = nextContext
			} else {
				contextNameQuestion := &survey.Select{
					Message: "Choose a context:",
					Options: contexts,
					Default: currentContext,
				}
				err = survey.AskOne(contextNameQuestion, &contextName)
				if err != nil {
					return err
				}
			}
			if contextName == "" {
				return nil
			}
			if contextName == currentContext {
				fmt.Printf("Context %q already selected.\n", currentContext)
				return nil
			}
			out, err := exec.Command("kubectl", "config", "use-context", contextName).CombinedOutput()
			if err != nil {
				return err
			}
			fmt.Print(string(out))
			conf.PreviousContext = currentContext
			return nil
		},
	}
	flags := cmd.PersistentFlags()
	flags.BoolVarP(&o.printsCurrentContext, "current", "c", false, "Show the current context name")
	return cmd
}
