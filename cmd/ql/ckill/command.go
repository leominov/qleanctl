package ckill

import (
	"context"
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "ckill <namespace> <service>",
		Short: "Kill CrashLoopBackOff pods",
		Long:  "Kill CrashLoopBackOff pods for a specific namespace service.",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 2 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			namespace := args[0]
			service := args[1]

			client, err := kubernetes.NewClientSet()
			if err != nil {
				return err
			}

			listOpts := metav1.ListOptions{
				LabelSelector: fmt.Sprintf("name=%s", service),
			}
			pods, err := client.CoreV1().Pods(namespace).List(context.Background(), listOpts)
			if err != nil {
				return err
			}
			if len(pods.Items) == 0 {
				fmt.Println("No pods found")
				return nil
			}

			var podList []string
			for _, pod := range pods.Items {
				for _, containerStatus := range pod.Status.ContainerStatuses {
					if !containerStatus.Ready {
						if containerStatus.State.Waiting != nil {
							if containerStatus.State.Waiting.Reason == "CrashLoopBackOff" {
								podList = append(podList, pod.Name)
								break
							}
						}
					}
				}
			}

			if len(podList) == 0 {
				fmt.Println("No CrashLoopBackOff pods found")
				return nil
			}

			podsInterface := client.CoreV1().Pods(namespace)
			for _, pod := range podList {
				err := podsInterface.Delete(context.Background(), pod, metav1.DeleteOptions{})
				if err != nil {
					return err
				}
				fmt.Printf("Pod %q deleted\n", pod)
			}
			return nil
		},
	}
	return cmd
}
