package ttl_set

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubeclient "k8s.io/client-go/kubernetes"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
)

type options struct {
	Service string
	Branch  string
	Days    int
}

func Command() *cobra.Command {
	opts := &options{}
	var cmd = &cobra.Command{
		Use:   "ttl:set <service> <branch> [days]",
		Short: "Set environment lifetime",
		Long:  "Set environment lifetime for specific branch.",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 2 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			client, err := kubernetes.NewClientSet()
			if err != nil {
				return err
			}

			opts.Service = args[0]
			opts.Branch = args[1]
			opts.Days = 4
			if len(args) > 2 {
				opts.Days, err = strconv.Atoi(args[2])
				if err != nil {
					return err
				}
			}

			return setServiceEnvironmentTTl(client, opts)
		},
	}
	return cmd
}

func setServiceEnvironmentTTl(client kubeclient.Interface, opts *options) error {
	configMapList, err := client.CoreV1().ConfigMaps("").List(context.Background(), metav1.ListOptions{
		LabelSelector: fmt.Sprintf("monitoring_scope,name=%s,branch=%s", opts.Service, opts.Branch),
	})
	if err != nil {
		return err
	}

	if len(configMapList.Items) == 0 || opts.Days == 0 {
		return nil
	}

	for _, cm := range configMapList.Items {
		if !strings.HasSuffix(cm.Name, "-cm") {
			continue
		}
		if _, ok := cm.Annotations["ci_project_id"]; !ok {
			continue
		}
		cm.Annotations["pltf_ttl"] = fmt.Sprintf("%dd", opts.Days)
		_, err = client.CoreV1().ConfigMaps(cm.Namespace).Update(context.Background(), &cm, metav1.UpdateOptions{})
		if err != nil {
			fmt.Printf("Failed to update %s/%s environment: %v\n", opts.Service, opts.Branch, err)
			continue
		}

		fmt.Printf("Lifetime %s/%s successfully updated\n", opts.Service, opts.Branch)
	}

	return nil
}
