package ttl_set

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
)

func TestCommand(t *testing.T) {
	k8sClient := fake.NewSimpleClientset(
		&corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "secret-a-cm",
				Namespace: "default",
				Labels: map[string]string{
					"monitoring_scope": "app",
					"name":             "service-a",
					"branch":           "branch-a",
				},
				Annotations: map[string]string{
					"pltf_ttl": "5d",
				},
			},
		},
		&corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "secret-b-cm",
				Namespace: "default",
				Labels: map[string]string{
					"name":   "service-b",
					"branch": "branch-b",
				},
			},
		},
		&corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "secret-c-cm",
				Namespace: "default",
				Labels: map[string]string{
					"monitoring_scope": "app",
					"name":             "service-c",
					"branch":           "branch-c",
				},
				Annotations: map[string]string{
					"ci_project_id": "id1",
				},
			},
		},
		&corev1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "secret-d-cm",
				Namespace: "default",
				Labels: map[string]string{
					"monitoring_scope": "app",
					"name":             "service-d",
					"branch":           "branch-d",
				},
				Annotations: map[string]string{
					"ci_project_id": "id1",
					"pltf_ttl":      "5d",
				},
			},
		},
	)

	err := setServiceEnvironmentTTl(k8sClient, &options{
		Service: "service-a",
		Branch:  "branch-a",
		Days:    2,
	})
	assert.NoError(t, err)
	cm, err := k8sClient.CoreV1().ConfigMaps("default").Get(context.Background(), "secret-a-cm", metav1.GetOptions{})
	if assert.NoError(t, err) {
		assert.Equal(t, "5d", cm.Annotations["pltf_ttl"])
	}

	err = setServiceEnvironmentTTl(k8sClient, &options{
		Service: "service-b",
		Branch:  "branch-b",
		Days:    2,
	})
	assert.NoError(t, err)
	cm, err = k8sClient.CoreV1().ConfigMaps("default").Get(context.Background(), "secret-b-cm", metav1.GetOptions{})
	if assert.NoError(t, err) {
		_, ok := cm.Annotations["pltf_ttl"]
		assert.False(t, ok)
	}

	err = setServiceEnvironmentTTl(k8sClient, &options{
		Service: "service-c",
		Branch:  "branch-c",
	})
	assert.NoError(t, err)
	cm, err = k8sClient.CoreV1().ConfigMaps("default").Get(context.Background(), "secret-c-cm", metav1.GetOptions{})
	if assert.NoError(t, err) {
		_, ok := cm.Annotations["pltf_ttl"]
		assert.False(t, ok)
	}

	err = setServiceEnvironmentTTl(k8sClient, &options{
		Service: "service-c",
		Branch:  "branch-c",
		Days:    2,
	})
	assert.NoError(t, err)
	cm, err = k8sClient.CoreV1().ConfigMaps("default").Get(context.Background(), "secret-c-cm", metav1.GetOptions{})
	if assert.NoError(t, err) {
		assert.Equal(t, "2d", cm.Annotations["pltf_ttl"])
	}

	err = setServiceEnvironmentTTl(k8sClient, &options{
		Service: "service-d",
		Branch:  "branch-d",
	})
	assert.NoError(t, err)
	cm, err = k8sClient.CoreV1().ConfigMaps("default").Get(context.Background(), "secret-d-cm", metav1.GetOptions{})
	if assert.NoError(t, err) {
		assert.Equal(t, "5d", cm.Annotations["pltf_ttl"])
	}

	err = setServiceEnvironmentTTl(k8sClient, &options{
		Service: "service-d",
		Branch:  "branch-d",
		Days:    10,
	})
	assert.NoError(t, err)
	cm, err = k8sClient.CoreV1().ConfigMaps("default").Get(context.Background(), "secret-d-cm", metav1.GetOptions{})
	if assert.NoError(t, err) {
		assert.Equal(t, "10d", cm.Annotations["pltf_ttl"])
	}
}
