package list

import (
	"context"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"

	"github.com/dustin/go-humanize"
	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/environment"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
)

type options struct {
	WithoutTTL bool
	WithTTL    bool
	Master     bool
}

func Command() *cobra.Command {
	opts := &options{}
	var cmd = &cobra.Command{
		Use: "list [service]",
		Aliases: []string{
			"ls",
		},
		Short: "List environments",
		Long:  "List environments for specific service.",
		RunE: func(cmd *cobra.Command, args []string) error {
			listOpts := metav1.ListOptions{
				LabelSelector: "monitoring_scope",
			}

			if len(args) > 0 {
				listOpts.LabelSelector = fmt.Sprintf("monitoring_scope,name=%s", args[0])
			}

			client, err := kubernetes.NewClientSet()
			if err != nil {
				return err
			}

			configMapList, err := client.CoreV1().ConfigMaps("").List(context.Background(), listOpts)
			if err != nil {
				return err
			}
			if len(configMapList.Items) == 0 {
				fmt.Println("Nothing found.")
				return nil
			}

			w := new(tabwriter.Writer)
			w.Init(os.Stdout, 0, 8, 0, '\t', 0)
			_, _ = fmt.Fprintln(w, "Service\tBranch\tProject ID\tTimestamp\tTTL")
			for _, cm := range configMapList.Items {
				if !strings.HasSuffix(cm.Name, "-cm") {
					continue
				}

				service := cm.Labels["name"]

				branch := cm.Labels["branch"]
				if !opts.Master && branch == "master" {
					continue
				}

				timestamp := cm.CreationTimestamp
				ttlRaw := cm.Annotations["pltf_ttl"]
				projectID := strings.TrimPrefix(cm.Annotations["ci_project_id"], "id")
				if projectID == "" {
					projectID = "-"
				}
				if ttlRaw == "" {
					if !opts.WithTTL {
						_, _ = fmt.Fprintf(w, "%s\t%s\t%s\t%s\t-\n", service, branch, projectID, timestamp)
					}
				} else if !opts.WithoutTTL {
					ttl, err := environment.ParseTTL(ttlRaw)
					if err == nil && branch != "master" {
						ttlRaw = fmt.Sprintf("%s (%s)", ttlRaw, humanize.Time(timestamp.Add(ttl)))
					}
					_, _ = fmt.Fprintf(w, "%s\t%s\t%s\t%s\t%s\n", service, branch, projectID, timestamp, ttlRaw)
				}
			}
			_ = w.Flush()
			return nil
		},
	}
	flags := cmd.PersistentFlags()
	flags.BoolVar(&opts.WithoutTTL, "without-ttl", false, "Prints only environments without TTL")
	flags.BoolVar(&opts.WithTTL, "with-ttl", false, "Prints only environment with TTL")
	flags.BoolVar(&opts.Master, "master", false, "Include environments with master branch")
	return cmd
}
