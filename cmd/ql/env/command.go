package env

import (
	"github.com/spf13/cobra"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/env/list"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/env/ttl_dec"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/env/ttl_inc"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/env/ttl_set"
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "env",
		Short: "*(BETA)* Toolchain for environments",
	}
	cmd.AddCommand(
		list.Command(),
		ttl_inc.Command(),
		ttl_dec.Command(),
		ttl_set.Command(),
	)
	return cmd
}
