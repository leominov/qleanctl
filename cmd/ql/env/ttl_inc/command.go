package ttl_inc

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubeclient "k8s.io/client-go/kubernetes"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
)

type options struct {
	Service string
	Branch  string
	Days    int
}

func Command() *cobra.Command {
	opts := &options{}
	var cmd = &cobra.Command{
		Use: "ttl:inc <service> <branch>",
		Aliases: []string{
			"prolong",
		},
		Short: "Increase environment lifetime",
		Long:  "Increase environment lifetime for specific branch.",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 2 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			client, err := kubernetes.NewClientSet()
			if err != nil {
				return err
			}

			opts.Service = args[0]
			opts.Branch = args[1]

			return increaseServiceEnvironmentTTl(client, opts)
		},
	}
	flags := cmd.PersistentFlags()
	flags.IntVar(&opts.Days, "days", 2, "Increase lifetime for specified days")
	return cmd
}

func increaseServiceEnvironmentTTl(client kubeclient.Interface, opts *options) error {
	configMapList, err := client.CoreV1().ConfigMaps("").List(context.Background(), metav1.ListOptions{
		LabelSelector: fmt.Sprintf("monitoring_scope,name=%s,branch=%s", opts.Service, opts.Branch),
	})
	if err != nil {
		return err
	}

	if len(configMapList.Items) == 0 || opts.Days == 0 {
		return nil
	}

	for _, cm := range configMapList.Items {
		if !strings.HasSuffix(cm.Name, "-cm") {
			continue
		}

		ttlDaysRaw, ok := cm.Annotations["pltf_ttl"]
		if !ok {
			fmt.Printf("Environment %s/%s without ttl\n", opts.Service, opts.Branch)
			continue
		}

		ttlDays, err := strconv.Atoi(strings.TrimSuffix(ttlDaysRaw, "d"))
		if err != nil {
			continue
		}

		ttlDays += opts.Days
		cm.Annotations["pltf_ttl"] = fmt.Sprintf("%dd", ttlDays)
		_, err = client.CoreV1().ConfigMaps(cm.Namespace).Update(context.Background(), &cm, metav1.UpdateOptions{})
		if err != nil {
			fmt.Printf("Failed to update %s/%s environment: %v\n", opts.Service, opts.Branch, err)
			continue
		}

		fmt.Printf("Lifetime %s/%s successfully increased\n", opts.Service, opts.Branch)
	}
	return err
}
