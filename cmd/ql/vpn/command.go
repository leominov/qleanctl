package vpn

import (
	"github.com/spf13/cobra"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/vpn/create"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/vpn/list"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/vpn/revoke"
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "vpn",
		Short: "Interact with VPN certificates",
	}
	cmd.AddCommand(
		create.Command(),
		revoke.Command(),
		list.Command(),
	)
	return cmd
}
