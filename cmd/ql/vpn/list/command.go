package list

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"

	kube "gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/vpn"
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "list",
		Short: "List of existing VPN certificates",
		Long:  "List of existing VPN certificates, using current Kubernetes context",
		RunE: func(cmd *cobra.Command, args []string) error {
			return run()
		},
	}
	return cmd
}

func run() error {
	kubeconfig, err := kube.GetClientConfig()
	if err != nil {
		return err
	}
	kubeclient, err := kube.NewClientSetWithConfig(kubeconfig)
	if err != nil {
		return err
	}
	certs, err := vpn.ListCertificates(kubeclient, kubeconfig)
	if err != nil {
		return err
	}
	fmt.Println(strings.Join(certs, "\n"))
	return nil
}
