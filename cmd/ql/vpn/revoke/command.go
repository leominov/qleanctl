package revoke

import (
	"errors"
	"fmt"

	"github.com/spf13/cobra"

	kube "gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/vpn"
)

type options struct {
	Name string
}

func Command() *cobra.Command {
	opts := &options{}
	var cmd = &cobra.Command{
		Use:   "revoke <name>",
		Short: "Revoke a VPN certificate",
		Long:  "Revoke a VPN certificate, using current Kubernetes context",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			opts.Name = args[0]
			return run(opts)
		},
	}
	return cmd
}

func run(opts *options) error {
	kubeconfig, err := kube.GetClientConfig()
	if err != nil {
		return err
	}
	kubeclient, err := kube.NewClientSetWithConfig(kubeconfig)
	if err != nil {
		return err
	}
	err = vpn.RevokeCertificate(kubeclient, kubeconfig, opts.Name)
	if err != nil {
		return err
	}
	fmt.Printf("Key %q revoked\n", opts.Name)
	return nil
}
