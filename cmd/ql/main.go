package main

import (
	"os"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/cronjob"

	"github.com/spf13/cobra"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/check_cert"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/ckill"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/create"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/ekill"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/env"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/kctx"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/version"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/vpn"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/config"
)

func rootCommand(conf *config.Config) *cobra.Command {
	cmd := &cobra.Command{
		Use:          "ql",
		SilenceUsage: true,
	}

	subcommands := []*cobra.Command{
		check_cert.Command(),
		ckill.Command(),
		create.Command(),
		ekill.Command(),
		env.Command(),
		kctx.Command(conf),
		version.Command(),
		vpn.Command(),
		cronjob.Command(),
	}

	cmd.AddCommand(subcommands...)

	return cmd
}

func main() {
	conf, _ := config.Load()
	cmd := rootCommand(conf)
	err := cmd.Execute()
	if err != nil {
		os.Exit(1)
	}
	_ = conf.Save()
}
