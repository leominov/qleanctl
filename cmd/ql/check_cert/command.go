package check_cert

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/spf13/cobra"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"

	kube "gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
)

const (
	crtSecretField = "tls.crt"
	keySecretField = "tls.key"
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "check-cert <namespace> [secrets]",
		Short: "Check certificate expiration",
		Long:  "Check certificate expiration for a specific namespace secrets.",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			namespace := args[0]

			client, err := kube.NewClientSet()
			if err != nil {
				return err
			}

			var secrets []string
			if len(args) == 1 {
				secrets, err = getCertificateSecretNames(client, namespace)
			} else {
				secrets = args[1:]
			}

			var failed bool
			for _, secret := range secrets {
				cert, err := getCertificate(client, namespace, secret)
				if err != nil {
					fmt.Printf("Failed to get certificate: %v\n", err)
					failed = true
					continue
				}

				fmt.Printf("Kubernetes Secret: %s\n", secret)
				fmt.Printf("DNS Names: %s\n", strings.Join(cert.DNSNames, ", "))
				if sn := cert.SerialNumber; sn != nil {
					fmt.Printf("Serial Number: %s\n", sn.String())
				}

				fmt.Printf("Valid Not Before: %s\n", cert.NotBefore)
				fmt.Printf("Valid Not After: %s\n", cert.NotAfter)

				now := time.Now().UTC()
				validNow := now.After(cert.NotBefore) && now.Before(cert.NotAfter)

				now2w := now.Add(14 * 24 * time.Hour)
				valid14d := now2w.After(cert.NotBefore) && now2w.Before(cert.NotAfter)

				now1m := now.Add(30 * 24 * time.Hour)
				valid30d := now1m.After(cert.NotBefore) && now1m.Before(cert.NotAfter)

				fmt.Printf("Valid (now/14d/30d): %v/%v/%v\n", validNow, valid14d, valid30d)

				if !validNow {
					fmt.Println("Validation failed")
					failed = true
				}
			}

			if failed {
				return errors.New("failed")
			}

			return nil
		},
	}
	return cmd
}

func getCertificate(clientset kubernetes.Interface, namespace, secretName string) (*x509.Certificate, error) {
	secret, err := clientset.CoreV1().Secrets(namespace).Get(context.Background(), secretName, v1.GetOptions{})
	if err != nil {
		return nil, err
	}
	crt, ok := secret.Data[crtSecretField]
	if !ok {
		return nil, fmt.Errorf("%s not found in secret", crtSecretField)
	}
	key, ok := secret.Data[keySecretField]
	if !ok {
		return nil, fmt.Errorf("%s not found in secret", keySecretField)
	}
	cert, err := tls.X509KeyPair(crt, key)
	if err != nil {
		return nil, err
	}
	if len(cert.Certificate) == 0 {
		return nil, errors.New("empty certificate list")
	}
	return x509.ParseCertificate(cert.Certificate[0])
}

func getCertificateSecretNames(clientset kubernetes.Interface, namespace string) ([]string, error) {
	var items []string
	secretList, err := clientset.CoreV1().Secrets(namespace).List(context.Background(), v1.ListOptions{
		FieldSelector: "type=kubernetes.io/tls",
	})
	if err != nil {
		return nil, err
	}
	for _, secret := range secretList.Items {
		items = append(items, secret.Name)
	}
	sort.Strings(items)
	return items, nil
}
