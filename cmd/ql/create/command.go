package create

import (
	"github.com/spf13/cobra"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/create/argocd"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/create/library"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/create/namespace"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/create/service"
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "create",
		Short: "Create infrastructure",
	}
	cmd.AddCommand(
		argocd.Command(),
		service.Command(),
		library.Command(),
		namespace.Command(),
	)
	return cmd
}
