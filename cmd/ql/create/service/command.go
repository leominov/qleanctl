package service

import (
	"github.com/spf13/cobra"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/create/shared"
)

const (
	golangCookiecutterRepository = "https://gitlab.qleanlabs.ru/platform/go-libs/app-boilerplate.git"
	nodejsCookiecutterRepository = "https://gitlab.qleanlabs.ru/platform/libraries/app-boilerplate.git"
)

var (
	languageToTemplateMap = map[string]string{
		"go":         golangCookiecutterRepository,
		"golang":     golangCookiecutterRepository,
		"node":       nodejsCookiecutterRepository,
		"nodejs":     nodejsCookiecutterRepository,
		"javascript": nodejsCookiecutterRepository,
		"js":         nodejsCookiecutterRepository,
	}
)

func Command() *cobra.Command {
	var cmd = shared.NewBasicCookiecutterCommand(languageToTemplateMap)
	cmd.Use = "service <service-name>"
	cmd.Short = "Create a new service"
	cmd.Long = "Create a new service, using cookiecutter's template"
	return cmd
}
