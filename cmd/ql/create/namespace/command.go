package namespace

import (
	"context"
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
)

var (
	requiredLabels = map[string]string{
		"qlean.ru/gatekeeper":          "true",
		"qlean.ru/kube-legacy-webhook": "true",
	}
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use: "namespace <namespace-name>",
		Aliases: []string{
			"ns",
		},
		Short: "Create a new Kubernetes namespace",
		Long:  "Create a new Kubernetes namespace for applications with required labels",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			namespaceName := args[0]

			client, err := kubernetes.NewClientSet()
			if err != nil {
				return err
			}

			namespace := &corev1.Namespace{
				ObjectMeta: metav1.ObjectMeta{
					Name:   namespaceName,
					Labels: requiredLabels,
				},
			}
			_, err = client.CoreV1().Namespaces().Create(context.Background(), namespace, metav1.CreateOptions{})
			if err != nil {
				return err
			}

			fmt.Printf("Namespace %q created\n", namespaceName)

			return nil
		},
	}
	return cmd
}
