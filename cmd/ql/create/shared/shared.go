package shared

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/cobra"
)

type options struct {
	Name                  string
	Language              string
	Checkout              string
	LanguageToTemplateMap map[string]string
}

func NewBasicCookiecutterCommand(languageToTemplateMap map[string]string) *cobra.Command {
	var opts = &options{}
	var cmd = &cobra.Command{
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		PreRunE: func(cmd *cobra.Command, args []string) error {
			if _, err := exec.LookPath("cookiecutter"); err != nil {
				return errors.New("command cookiecutter not found. Please install it manually and retry")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			opts.Name = args[0]
			if opts.Language == "" {
				contextNameQuestion := &survey.Select{
					Message: "Choose a language:",
					Options: []string{"Go", "Node"},
				}
				err := survey.AskOne(contextNameQuestion, &opts.Language)
				if err != nil {
					return err
				}
			}
			opts.Language = strings.ToLower(opts.Language)
			opts.LanguageToTemplateMap = languageToTemplateMap
			return run(opts)
		},
	}
	flags := cmd.PersistentFlags()
	flags.StringVarP(&opts.Language, "language", "l", "", "Programming language (Go or Node)")
	flags.StringVarP(&opts.Checkout, "checkout", "c", "cookiecutter", "Branch, tag or commit to checkout after git clone")
	return cmd
}

func run(opts *options) error {
	repo, ok := opts.LanguageToTemplateMap[opts.Language]
	if !ok {
		return fmt.Errorf("template for %s language not found", opts.Language)
	}

	cmd := exec.Command("cookiecutter", []string{
		fmt.Sprintf("--checkout=%s", opts.Checkout),
		repo,
		fmt.Sprintf("service_name=%s", opts.Name),
	}...)
	cmd.Stdout = os.Stdout
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}

	fmt.Printf("New service %s created\n", opts.Name)
	return nil
}
