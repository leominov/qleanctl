package argocd

import (
	"bytes"
	"errors"
	"fmt"
	"os"
	"sort"
	"strings"
	"text/template"

	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"
)

const (
	applicationTemplate = `---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: {{.Name}}
spec:
  {{if eq .Cluster "kube-kosmos-prod"}}project: kosmos{{else}}project: default{{end}}
  destination:
    {{if .Cluster}}name: {{.Cluster}}{{else}}server: https://kubernetes.default.svc{{end}}
    {{if .Namespace}}namespace: {{.Namespace}}{{end}}
  source:
    repoURL: {{.Repository}}
    targetRevision: HEAD
    path: {{.Path}}
`
)

var (
	qs = []*survey.Question{
		{
			Name: "namespace",
			Prompt: &survey.Input{
				Message: "Namespace:",
			},
		},
		{
			Name: "repository",
			Prompt: &survey.Input{
				Message: "Repository:",
				Default: "git@gitlab.qleanlabs.ru:platform/infrastructure.git",
			},
			Validate: survey.Required,
		},
		{
			Name: "path",
			Prompt: &survey.Input{
				Message: "Path:",
			},
			Validate: survey.Required,
		},
	}

	applicationSuffixToClusterMap = map[string]string{
		"kosmos":  "kube-kosmos-prod",
		"preprod": "kube-pltf-preprod",
		"prod":    "kube-pltf-prod",
		"stage":   "kube-pltf-stage",
	}
)

type options struct {
	Name       string
	Filename   string
	Cluster    string
	Namespace  string
	Repository string
	Path       string
}

type kustomization struct {
	Bases []string `yaml:"bases"`
}

func Command() *cobra.Command {
	k := &kustomization{}
	opts := &options{}
	var cmd = &cobra.Command{
		Use:   "argocd <application-name>",
		Short: "Create a new ArgoCD application",
		Long:  "Create a new ArgoCD application, using wizard",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 1 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		PreRunE: func(cmd *cobra.Command, args []string) error {
			err := k.LoadFromFile("kustomization.yaml")
			if err != nil {
				return err
			}
			opts = &options{
				Name:     strings.ToLower(args[0]),
				Filename: strings.ToLower(args[0]) + ".Application.yaml",
			}
			if k.IsFileExists(opts.Filename) {
				return fmt.Errorf("application %s already exists", opts.Name)
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			err := survey.Ask(qs, opts)
			if err != nil {
				return err
			}
			for suffix, cluster := range applicationSuffixToClusterMap {
				if strings.HasSuffix(opts.Name, suffix) {
					opts.Cluster = cluster
					break
				}
			}
			tmpl, err := template.New("app").Parse(applicationTemplate)
			if err != nil {
				return err
			}
			var page bytes.Buffer
			err = tmpl.Execute(&page, opts)
			if err != nil {
				return err
			}
			err = os.WriteFile(opts.Filename, page.Bytes(), 0644)
			if err != nil {
				return err
			}
			k.Bases = append(k.Bases, opts.Filename)
			fmt.Printf("New application %s created\n", opts.Name)
			return k.WriteToFile("kustomization.yaml")
		},
	}
	return cmd
}

func (k *kustomization) IsFileExists(filename string) bool {
	for _, base := range k.Bases {
		if base == filename {
			return true
		}
	}
	return false
}

func (k *kustomization) LoadFromFile(filename string) error {
	b, err := os.ReadFile(filename)
	if err != nil {
		return err
	}
	return yaml.Unmarshal(b, k)
}

func (k *kustomization) WriteToFile(filename string) error {
	sort.Strings(k.Bases)
	b, err := yaml.Marshal(k)
	if err != nil {
		return err
	}
	b = append([]byte("---\n"), b...)
	return os.WriteFile(filename, b, 0644)
}
