package suspend

import (
	"context"
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	kube "gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/kubernetes"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/types"
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "suspend <namespace> <cronjob>",
		Short: "Suspend CronJob",
		Long:  "Suspend a specific namespace CronJob.",
		Args: func(cmd *cobra.Command, args []string) error {
			if len(args) < 2 {
				return errors.New("not enough arguments passed")
			}
			return nil
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			namespace := args[0]
			cronjobName := args[1]

			clientset, err := kube.NewClientSet()
			if err != nil {
				return err
			}

			cronjob, err := clientset.BatchV1beta1().CronJobs(namespace).Get(context.Background(), cronjobName, v1.GetOptions{})
			if err != nil {
				return fmt.Errorf("failed to get cronjob: %v", err)
			}

			if cronjob.Spec.Suspend != nil && *cronjob.Spec.Suspend == true {
				return fmt.Errorf("cronjob %s already suspended", cronjobName)
			}

			cronjob.Spec.Suspend = types.Bool(true)

			_, err = clientset.BatchV1beta1().CronJobs(namespace).Update(context.Background(), cronjob, v1.UpdateOptions{})
			if err != nil {
				return fmt.Errorf("failed to update cronjob: %v", err)
			}

			fmt.Printf("CronJob %s suspended\n", cronjobName)

			return nil
		},
	}
	return cmd
}
