package cronjob

import (
	"github.com/spf13/cobra"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/cronjob/resume"
	"gitlab.qleanlabs.ru/platform/infra/qleanctl/cmd/ql/cronjob/suspend"
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "cronjob",
		Short: "Interact with Kubernetes CronJobs",
	}
	cmd.AddCommand(
		resume.Command(),
		suspend.Command(),
	)
	return cmd
}
