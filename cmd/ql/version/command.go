package version

import (
	"fmt"

	"github.com/spf13/cobra"

	"gitlab.qleanlabs.ru/platform/infra/qleanctl/pkg/version"
)

func Command() *cobra.Command {
	var cmd = &cobra.Command{
		Use:   "version",
		Short: "Get qleanctl version",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(version.GetVersion())
		},
	}
	return cmd
}
