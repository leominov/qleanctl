# Qlean CLI

## Usage

```
Usage:
  ql [command]

Available Commands:
  check-cert  Check certificate expiration
  ckill       Kill CrashLoopBackOff pods
  completion  generate the autocompletion script for the specified shell
  create      Create infrastructure
  ekill       Kill Evicted pods
  env         *(BETA)* Toolchain for environments
  help        Help about any command
  kctx        Switch Kubernetes context
  version     Get qleanctl version
  vpn         Interact with VPN certificates

Flags:
  -h, --help   help for ql

Use "ql [command] --help" for more information about a command.
```

## Links

* https://console.cloud.google.com/storage/browser/devops-internal/ql;tab=objects?pageState=(%22StorageObjectListTable%22:(%22f%22:%22%255B%255D%22))&project=qlean-242314&prefix=&forceOnObjectsSortingFiltering=false
